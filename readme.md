#Apium - Celery Task Manager

## Features
I wrote apium (*genus name of celery*) to keep track of my celery tasks. It has the following features.

* Celery Task Manager. Submit your celery async result objects (aos)) to apium and it will keep track of their progress.
* Get status of all submited celery aos in one go (you can also get state of just one.)
* **Funnel the results of an ao into anther function *after* the successful celery finishes the execution.**
* Runs in the background.
* Lightweight and customizable.

## Getting Started

Instantiate apium and start the timer in the background.
~~~
import apium
ctm = apium.CeleryTaskManager()
ctm.startCycle()
~~~


Run your function through celery. In this case the function is test.add

~~~
ao = test.add.delay(1)
~~~

Optionally create the function that accespts the results of the above.

~~~
def after_add(val):
	r = val + 1000
	return r
~~~

Then submit the celery async result object (ao) to apium.

~~~
id = ctm.add(ao,name = "name_here",post_run_func = func1, interval = 10)
~~~

Thats it!. You can continue add other ao s. id is the id of the ao. You can use the id to access information including returned value.

To get the status...

~~~
rdict, completed_dict = ctm.getState()
~~~

rdict contains information on the pending / running ao s. 
completed_dict contains info on completed aos. 



~~~
print rdict #format = {id|task_name|given_name = ao id}
>>>{'00d266c8-1442-4898-b752-fb1dfa8773b1|test.add|no_name_specified': ('PENDING',<apium.asyncInstance instance at 0x103e6a320>), ...


print completed_dict #format {id:ao object}
>>> {'328e965e-a8b6-484c-8d6f-bb0ae7f19799': <AsyncResult: 328e965e-a8b6-484c-8d6f-bb0ae7f19799> ...
~~~

To get the value returned from the function.

Wait until you get Success in getState()
get completed_dict and extract the specific ao object using the id returned when the ao was first submited.

Then look at the class variables.

~~~
completed_ao = completed_dict[id]
value_returned_by_function = completed_ao.obj.get() #use .obj. to get to the original ao object.

value_from_post_func = completed_ao.post_run_func_output #values returned by the post func triggered after successful execution by celery



~~~


