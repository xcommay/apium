#apium: celery task manager

import threading

class CeleryTaskManager():
    """
    apium - Celery Task Manager (ctm)
    This little class allows you to monitor your celery tasks automatically.
    
    Process:
    1. instantiate. (ctm )
    2. startCycle() - stats the independent thread timer.
    3. submit your function / params to celery and get the async result object (ao).
    4. add it to the instantiated ctm. ctm.add(ao, )
    Thats it!
    ctm will automatically monitor your celery processes.
    To get info: ctm.getState()
    
    *Bonus Feature*
    When adding an ao to ctm (ctm.add) you can optionally specify a function to be run on the results for successfully completed asyn result objects. 
    
    Parameters
    ----------
    one_cycle_length : length of one cycle
    
    Returns
    -------
    ctm: celery task manager object
  
    """
    def __init__(self,one_cycle_length = 1):
        self.one_cycle = one_cycle_length
        self.timer = threading.Timer(one_cycle_length, self.cycler)
        self.counter = 0
        self.ao_dict = {} #{ao_id:ao_obj}
        self.ao_trigger_counts = {} #{ao_id:ao.trigger_count}
        self.completed_ao_dict = {}
        self.running = False
        
   
    def startCycle(self):
        """
        Starts the thread.Timer of the ctm.
        """
        self.timer.start()
        self.running = True

    
    def stopCycle(self):
        """
        Stops the thread.Timer of the ctm.
        """
        self.timer.cancel()
        self.running = False


    def cycler(self):
        self.worker()
        self.timer = threading.Timer(self.one_cycle, self.cycler)
        self.timer.start()
    
    def resetCounter(self):
        """
        Resets the built in counter (self.counter) of the ctm object.
        """
        self.counter = 0
        #reset trigger counts of ao_id
        for ao_id in self.ao_trigger_counts:
            self.ao_trigger_counts[ao_id] = 0
        
            
    def add(self, ao_obj, name = "no_name_specified" , post_run_func = None, opt_var_dict = None, interval = 1):
        """
        Adds a new celery async result object to ctm. 

        Parameters
        ----------
        ao_obj: celery async result object
        name: custom name for ao_obj. Does not need to be unique
        post_run_func: optional, a function object where the result will be sent to. (trigger function)
        interval: How often should I check the state of the selery ao_obj. seconds.

        
        """
        ao = asyncInstance(ao_obj, name, post_run_func,opt_var_dict, interval)
        ao.start_count = self.counter
        #see if this ao has been already added 
        if ao.id in self.ao_dict:
            #this is a duplicate
            print "Duplicate"
            return 0
        else:
            self.ao_dict[ao.id] = ao
            self.ao_trigger_counts[ao.id] = self.counter + interval
            print "ao added"
            return ao.id
    
        
    def worker(self):
        """
        Workhorse of ctm.
        1. manages counter
        2. identifies ao who's states needs checking and does that.
        3. gets the state and "retires" the ao's that have been completed.
        """
        #print 'tick'
        self.counter = self.counter + 1
        if self.counter >= 3600:
            self.resetCounter()
        
        #check for ao with trigger matching current counter
        keys = self.ao_trigger_counts.keys()
        for ao_id in keys:
            if self.ao_trigger_counts[ao_id] <= self.counter:
                #check the state of the ao
                state,ao = self.getState(ao_id)
                if state == "SUCCESS":
                    #the process has completed successfully
                    #run the post_run_func if any
                    output = ao.obj.get()
                    f = ao.post_run_func
                    if not f == None:
                        if  ao.opt_var_dict ==  None:
                            ao.post_run_func_output = f(output)
                        else:
                            ao.post_run_func_output = f(output, **ao.opt_var_dict)

                        print "func executed"
                    self.retireAo(ao_id)
                elif state == "FAILURE":
                    ao.traceback = ao.obj.traceback
                    self.retireAo(ao_id)
    
    def forget(ao_id):
        """
        tries to removed an ao_id from celery
        Only works on some backends
        """
        ao = self.ao_dict[ao_id]
        try:
            ao.obj.forget()
            return "removed"
        except:
            return "error"
    
    def retireAo(self,ao_id):
        """
        Removes completed/ failed ao s from ctm.
        """
        ao = self.ao_dict[ao_id]
        ao.stop_count = self.counter
        #add ao to completed dict
        self.completed_ao_dict[ao_id] = ao
        #remove ao from dicts
        del(self.ao_trigger_counts[ao_id])
        del(self.ao_dict[ao_id])
        
    
    def getState(self,ao_id = None):
        """
        Returns the state of ao objects.
        
        Parameters
        ----------
        ao_id: optional, id of ao. if None, provides info on all ao s.
        
        Returns
        -------
        rdict: dictionary of pending / running ao s. {id|task_name|given_name = ao id}
        completed_ao_dict: dict of completed ao s. {id:ao object}. To get value do ao.get()
        """
        if ao_id == None:
            #do for all
            rdict = {}
            keys = self.ao_dict.keys()
            for ao_id in keys :
                ao = self.ao_dict[ao_id]
                lapsed_time = ao.stop_count - ao.start_count
                
                k = str(ao_id) + "|" + str(ao.task_name) + "|" + str(ao.name) + "|" + str(lapsed_time)
                v = self.getState(ao_id)
                rdict[k] = v
            return rdict, self.completed_ao_dict
                
                
            
        else:
            #a single ao has been specified
            ao = self.ao_dict[ao_id]
            ao.last_known_state = ao.obj.state
            return  ao.last_known_state, ao
        

class asyncInstance():
    """
    Class for storing ao information. For internal use
    """
    def __init__(self, obj, name = "no_name_specified", post_run_func = None, opt_var_dict=None,interval = 1):
        self.obj = obj
        self.post_run_func = post_run_func
        self.opt_var_dict = opt_var_dict
        self.interval = interval
        self.name = name
        self.trigger_count = interval + 5 #the next counter value to trigger the count
        self.id = obj.id
        self.task_name = obj.task_name
        self.last_known_state = "init"
        self.traceback = None
        self.post_run_func_output = None
        self.start_count = 0
        self.stop_count = 0
        

    
       
